# -*- coding: utf-8 -*-
###################################################
#                                                 #
# Density Searching Script for Golly By 黃于軒     #
#                                                 #
# The result is in csv format at ./result-csv.txt #
#                                                 #
###################################################

import random
import golly

##### Settings #####
repeattimes = 10000 # how many times we're going to run
generations = ( 100, 1001, 100 ) # how many times we're going to iterate
                                 # format:(start,stop,step)
width = 20
height = 20
rule = 'b3/s23:p20,20'
filename = 'result-csv.txt' # output filename
writemode = 'a' # 'a' for append and 'w' for overwrite
####################

##### Init #####
golly.note( 'Start!!' )

golly.new( 'Calculating' ) # creates a new universe

golly.setrule( rule )

size = width*height

cells = []
for i in range( -width/2 if width%2 == 0 else -width/2+1
                , width/2 if width%2 == 0 else width/2+1 ):
    for j in range( -height/2 if height%2 == 0 else -height/2+1
                    , height/2 if height%2 == 0 else height/2+1 ):
        cells.append( [i,j] ) # put all the positions in one list
                              # so we can shuffle it to randfill later

random.seed()

golly.select( [-width/2 if width%2 == 0 else -width/2+1
               ,-height/2 if height%2 == 0 else -height/2+1
               ,width,height] ) # select all
                                                  # so we can randfill later
#################

def randfill(density):  # make our own randfill function
                        # since the one in golly isn't that accurate
    golly.clear( 0 ) # clear all in selection
                     # (we selected all before so that means 'clear all')
    random.shuffle( cells ) # shuffle the list
    livecells = float( float( size ) * ( float( density ) / 100 ) ) # calculate the number of cells we should put
    for i in range( 0, int ( livecells ) ):
        golly.setcell( cells[i][0], cells[i][1], 1) # get the first few elements
                                                    # and set their status to ALIVE

##### Main Program #####
with open( filename, writemode ) as file:
    
    for h in range( generations[0], generations[1], generations[2] ):
        
        file.write( '\n' \
                    + '"' \
                    + 'Rule:' \
                    + rule # wrap the rule in double-quotes so we can escape the comma
                    + '"'
                    + '\nGenerations:' \
                    + str( h ) \
                    + '\nRepeatTimes:' \
                    + str( repeattimes ) \
                    + '\n' ) # write settings

        for i in range( 1, 100 ):
            file.write( str(i) + ',' ) # write initial randfill density
        
            for j in range( repeattimes+1 ):
                golly.show( str( j ) + '/' + str( i ) + '/' + str( h ) ) # show where we're at now
                randfill( i )
                golly.run( h ) # run for a number of generations
                density = float( golly.getpop() ) / ( float( size ) )
                file.write( str( density ) + ',' ) # write final density

            file.write( ',"=AVERAGE(OFFSET(INDIRECT(ADDRESS(ROW(), 2)),0,0,1,' + str( repeattimes ) + '))",' ) # write excel average function
            file.write( '"=MEDIAN(OFFSET(INDIRECT(ADDRESS(ROW(), 2)),0,0,1,' + str( repeattimes ) + '))",' ) # median function
            file.write( '"=QUARTILE(OFFSET(INDIRECT(ADDRESS(ROW(), 2)),0,0,1,' + str( repeattimes ) + '),1)",' ) # 25% function (QUARTILE.INC in excel 2010 or later)
            file.write( '"=QUARTILE(OFFSET(INDIRECT(ADDRESS(ROW(), 2)),0,0,1,' + str( repeattimes ) + '),3)",' ) # 75% function (QUARTILE.INC in excel 2010 or later)
            file.write( '"=MIN(OFFSET(INDIRECT(ADDRESS(ROW(), 2)),0,0,1,' + str( repeattimes ) + '))",' ) # min function
            file.write( '"=MAX(OFFSET(INDIRECT(ADDRESS(ROW(), 2)),0,0,1,' + str( repeattimes ) + '))",' ) # max function
            file.write( '"=STDEV(OFFSET(INDIRECT(ADDRESS(ROW(), 2)),0,0,1,' + str( repeattimes ) + '))",' ) # stdev function (STDEV.S in excel 2010 or later)
            file.write( '\n' )
    
golly.note( 'End!!' )
##### End #####
