# A script to auto open wlan using telnet on zyxel p880
import telnetlib
import time

def read_until( tn, string ): 
    if string in tn.read_until( string, 1 ):
        return
    else:
        raise Exception()

while( True ):
    try:
        tn = telnetlib.Telnet( "192.168.1.1" )

        read_until( tn, "Login: " )
        tn.write( "cht\n" ) # username

        read_until( tn, "Password: ")
        tn.write( "chtsvdsl\n" ) # password

        read_until( tn, "2. LAN/WLAN" )
        tn.write( "2\n" ) # LAN/WLAN
            
        while( True ):
            time.sleep(5)

            read_until( tn, "4. Show WLAN" )
            tn.write( "4\n" ) # Current status
            expect = tn.expect( [ "Disabled", "Enabled" ], 1 )[0]
            if expect == 1: # If wlan is enabled
                tn.write( "\n" )
                print "Stay Enabled"
                continue
            if expect == -1:
                raise Exception()
            print "Disabled -> Enabled"
            tn.write( "\n" )
            read_until( tn, "3. Configure WLAN" )
            tn.write( "3\n" ) # Configure WLAN

            read_until( tn, "[0-no, 1-yes]" )
            tn.write( "1\n" ) # yes

            read_until( tn, "Hit <enter> to continue" )
            tn.write( "\n" )
    except:
        print "error"
        tn.close()
        continue