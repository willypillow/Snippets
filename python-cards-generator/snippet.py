cards = int(raw_input("Cards:"))
maxNo = 2 ** cards # This is actually [Max Number]+1
mask = 1 # Mask for bitwise "and" to filter the last bit
output = []

# Init output
for i in range(0, cards):
    output.append([])

for i in range(1, maxNo):
    num = i
    for j in range(0, cards):
        if num & mask == 1:
            output[j].append(i)
        num >>= 1

print output