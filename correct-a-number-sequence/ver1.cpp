#include <iostream>

int atoi(char const *str) {
  int n = 0;
  while (*str != '\0') {
    if (!isdigit(*str)) return -1;
    n = n * 10 + *str++ - '0';
  }
  return n;
}

int main() {
  char strs[5000][100];
  int len = 0, realI = 1;
  while (std::cin >> strs[len++]);
  for (int i = 0; i < len && realI <= 2500; i++) {
    if (atoi(strs[i]) != realI) {
      int j = i;
      while (atoi(strs[j]) + 1 != atoi(strs[j + 1]) || atoi(strs[j]) <= realI) j++;
      std::cout << realI << ", ";
      for (int k = i; k < j; k++)  std::cout << strs[k] << ' ';
      std::cout << "->";
      for (int k = realI; k < atoi(strs[j]); k++) std::cout << k << ' ';
      std::cout << '\n';
      i = j;
      realI = atoi(strs[j]);
    }
    realI++;
  }
}
