#include <iostream>
#include <cstring>

int getInt(char *str) {
  char *end;
  if (!(std::cin >> str)) return -1;
  int in = strtol(str, &end, 10);
  if (*end != '\0') return 0;
  return in;
}

int main() {
  int in;
  char str[100];
  for (int i = 0, realI = 1; (in = getInt(str)) != -1; i++, realI++) {
    if (in != realI) {
      char err[100], replace[100] = "";
      sprintf(err, " %s", str);
      int j, newIn, lastIn = in, add = 0;
      for (j = i + 1;; j++) {
        newIn = getInt(str);
        if ((unsigned)(newIn - realI - 1) < 5u || lastIn == newIn - 1) break;
        sprintf(err + strlen(err), " %s", str);
        lastIn = newIn;
      }
      if (lastIn == newIn - 1 && realI < newIn) {
        for (int k = strlen(err) - 1; err[k] != ' '; k--) err[k] = '\0';
        newIn--;
        add = 1;
      }
      for (int k = realI; k < newIn; k++) sprintf(replace + strlen(replace), " %d", k);
      std::cout << realI << "," << err << "->" << replace << '\n';
      i = j;
      realI = newIn + add;
    }
  }
}
