# A tracker that acts as a proxy between you and the real tracker
# It modifies the ratio being passed to the tracker (aka ratio cheating)
# NOT TESTED!!
# I do not hold any responsiblities on this piece of code
# Do not use this on private trackers or you may get banned
# You have been warned
import BaseHTTPServer
import urlparse
import httplib

### Config ###
uRate = 1.0
dRate = 1.0
port = 8080
# Basically if the tracker URL is
# "http://domain.name:123/some/thing/announce"
# then trackerHost = "domain.name:123"
# and trackerPath = "/some/thing/announce"
trackerHost = ""
trackerPath = ""

class Handler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(self):
        # Get the get query
        query = urlparse.urlparse(self.path).query
        # Translate it into a dictionary
        get = urlparse.parse_qs(query)
        
        # Multiply the dl & up rate
        newDl = int(float(get["downloaded"][0]) * dRate)
        newUp = int(float(get["uploaded"][0]) * uRate)
        newQuery = query.replace("uploaded=" + get["uploaded"][0]
                      , "uploaded=" + str(newDl))
        newQuery = newQuery.replace("downloaded=" + get["downloaded"][0]
                      , "downloaded=" + str(newDl))

        # Get the request headers out in a dictionary
        head = {}
        for name, value in self.headers.items():
            head[name] = value
        # Overwrite the host data
        head["host"] = trackerHost

        # Modify whatever data you want to modify here
        # e.g peer-id user-agent etc
        # https://wiki.theory.org/BitTorrent_Tracker_Protocol for more info

        # Send the modified data to the real tracker
        con = httplib.HTTPConnection(trackerHost)
        con.request("GET", trackerPath + "?" + newQuery, None, head)
        resp = con.getresponse()
        head = resp.getheaders()
        body = resp.read()

        # Forward data back to client
        self.send_response(resp.status, resp.reason)
        for name, value in head:
            self.send_header(name, value)
        self.end_headers()
        self.wfile.write(body)

if __name__ == "__main__":
    # Start the HTTP server
    hnd = BaseHTTPServer.HTTPServer
    httpd = hnd(("localhost", port), Handler)

    print "Started on http://localhost:" + str(port) + "/"

    # Serve until a ^C
    try:
        httpd.serve_forever()
    except:
        pass

    # Close server
    httpd.server_close()
