#include <iostream>
#include <limits>

// Defines the number of rows and columns
const int ROWLEN = 10;
const int COLLEN = 10;

const int DEAD = 0;
const int ALIVE = 1;

int cell[ROWLEN][COLLEN];
int celltemp[ROWLEN][COLLEN]; // Global arrays that store the cell status

void initCell(); // Sets the starting cells with input from the user (using stdin)
int sum(int row, int col); // Calculates the number of alive neighbors for a cell
void printCell(); // Prints the cells to the console(stdout)
void run(); // Where the real calculation happens

// Pauses the program until a key is pressed
inline void pause()
{
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cin.get();
    return;
}

int main()
{
    std::cout << "--------Game Of Life---------" << std::endl;

    // Init...
    initCell();
    printCell();

    char mode = '\0';
    std::cout << "1 or 2?:"; // 1 for interactive and 2 for going certin number of turns
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cin.get(mode);

    std::cout << "Press any key to start..." << std::endl;
    pause();

    switch(mode)
    {
    // Interactive mode
    case '1':
    {
        char again = '\0';
        while(true)
        {
            run();
ReEnter:
            std::cout << "\nContinue?(y/n)";
            std::cin >> again;
            switch(again)
            {
            case 'y':
            {
                // Go back to the beginning of the loop
                continue;
            }
            case 'n':
            {
                // Drop to the break below...
                break;
            }
            default:
            {
                // Wrong input, go back
                // Used "goto" here since IMO it's easier & shorter for this purpose
                std::cout << "Input incorrect!" << std::endl;
                goto ReEnter;
            }
            }
            // If 'n' is entered above, it'll be dropped here
            break;
        }
        break;
    }

    // Certin number of turns
    case '2':
    {
        int times;
        std::cout << "Enter the times you want to run :";
        std::cin >> times;
        for(int i=0; i<times; i++)
        {
            run();
        }
        break;
    }
    default:
    {
        break;
    }
    }
    std::cout << "END!!" << std::endl;
    pause();
    return 0;
}

// Sets the starting cells with input from the user (using stdin)
void initCell()
{
    // Set all cells as dead
    for(int row = 0; row < ROWLEN; row++)
    {
        for(int col = 0; col < COLLEN; col++)
        {
            cell[row][col] = DEAD;
        }
    }

    // Start to set cell positions
    std::cout << "Please enter the position of cells. Type(-1,-1) to end.\n";
    int row = 0, col = 0;

    while(true)
    {
        std::cout << ">";
        std::cin >> row >> col;
        if(row >= 0 && row < ROWLEN && col >= 0 && col < COLLEN) // If row & col are in range...
        {
            cell[row][col] = ALIVE;
            row = 0;
            col = 0;
        }
        else if(row == -1 && col == -1) // Or is it an ending command...
        {
            row = 0;
            col = 0;
            break;
        }
        else // Wrong input
        {
            std::cout << "Input incorrect!" << std::endl;
        }
    }
}

// Calculates the number of alive neighbors for a cell
int sum(int row, int col)
{
    int count = 0;
    for(int r = row-1; r <= row+1; r++)
    {
        for(int c = col-1; c <= col+1; c++)
        {
            if(r < 0 || r >= ROWLEN || c < 0|| c >= COLLEN) // If the position is not in range, skip it
            {
                continue;
            }
            if(cell[r][c] == ALIVE)
            {
                count++;
            }
        }
    }
    if(cell[row][col] == ALIVE)
    {
        // Because the cell itself is calculated in the loop above,
        // so we have to subtract count by 1 if the cell is alive
        count--;
    }
    return count;
}

// Prints the cells to the console(stdout)
void printCell()
{
    std::cout << "\nCell status:\n";

    // Display the top line of the grid
    std::cout << "┌";
    for(int col = 0; col < COLLEN-1; col++)
    {
        std::cout << "─┬";
    }
    std::cout << "─┐\n";

    for(int row = 0; row < ROWLEN; row++)
    {
        std::cout << "│";
        for(int col = 0; col < COLLEN; col++)
        {
            switch(cell[row][col])
            {
            case ALIVE:
            {
                std::cout << "●";
                break;
            }
            case DEAD:
            {
                std::cout << "○";
                break;
            }
            default:
            {
                // Shouldn't happen!
                std::cerr << "Error!!";
            }
            }
            std::cout << "│";
        }

        std::cout << "\n";

        if(row < ROWLEN-1)
        {
            std::cout << "├";
            for(int col = 0; col < COLLEN-1; col++)
            {
                std::cout << "─┼";
            }
            std::cout << "─┤\n";
        }
    }

    // Display the bottom line of the grid
    std::cout << "└";
    for(int col = 0; col < COLLEN-1; col++)
    {
        std::cout << "─┴";
    }
    std::cout << "─┘" << std::endl;
}

// Where the real calculation happens
void run()
{
    int count = 0;
    for (int row = 0; row < ROWLEN; row++)
    {
        for(int col = 0; col < COLLEN; col++)
        {
            switch(sum(row, col))
            {
            // All changes are made in "celltemp" which we'll copy back to "cell" later
            case 2:
            {
                // If neighbors count is 2, then set the cell to what it is now
                // (The S2 in B3/S23)
                celltemp[row][col] = cell[row][col];
                break;
            }
            case 3:
            {
                // If neighbors count is 3, then set it as alive
                // since if the cell is alive, it stays that way
                // (The S3 in B3/S23)
                // otherwise if it is dead, it reborns
                // (The B3 in B3/S23)
                // thus, the cell would be alive regardless of its current status
                celltemp[row][col] = ALIVE;
                break;
            }
            default:
            {
                // Otherwise, set the cell as dead
                celltemp[row][col] = DEAD;
                break;
            }
            }
        }
    }

    // Copies from celltemp to cell and calculates the total population
    for(int row = 0; row < ROWLEN; row++)
    {
        for(int col = 0; col < COLLEN; col++)
        {
            cell[row][col] = celltemp[row][col];
            if(cell[row][col] == ALIVE)
            {
                // If this cell is alive
                count++;
            }
        }
    }
    printCell();
    std::cout << "Currently " << count << " cells alive." << std::endl;
}
