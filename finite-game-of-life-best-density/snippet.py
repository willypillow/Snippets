width = 5
height = 5
density = (
    1*4
    +0.6*(width-2)*2
    +0.6*(height-2)*2
    +0.375*(width-2)*(height-2)
    )/(width*height)
print density