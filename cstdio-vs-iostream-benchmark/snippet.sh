ubuntu@ubuntu:~$ lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 15.10
Release:	15.10
Codename:	wily

ubuntu@ubuntu:~$ uname -a
Linux ubuntu 4.4.0 #1 SMP Sun Jan 24 23:40:06 MST 2016 i686 i686 i686 GNU/Linux

ubuntu@ubuntu:~$ cat gen.scala
var i = 0
while (i < 1000000) {
  println(scala.util.Random.nextInt)
  i += 1
}

ubuntu@ubuntu:~$ scala gen.scala > input.txt

ubuntu@ubuntu:~$ cat gen2.scala
var i = 0
while (i < 10000000) {
  println(scala.util.Random.nextInt)
  i += 1
}

ubuntu@ubuntu:~$ scala gen2.scala > input2.txt

ubuntu@ubuntu:~$ cat printf.cpp
#include <cstdio>

int main() {
  int parity = 0, x;

  while (1 == scanf("%d", &x)) parity ^= x;
  printf("%d", parity);

  return 0;
}

ubuntu@ubuntu:~$ cat iostream.cpp
#include <iostream>

int main() {
  int parity = 0, x;

  std::cin.tie(0);
  std::ios_base::sync_with_stdio(0);

  while (std::cin >> x) parity ^= x;
  std::cout << parity;

  return 0;
}

ubuntu@ubuntu:~$ g++ --version
g++ (Ubuntu 5.2.1-22ubuntu2) 5.2.1 20151010
Copyright (C) 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

ubuntu@ubuntu:~$ g++ -lm -lmcrypt -O2 -std=c++11 -pipe -DONLINE_JUDGE -o printf printf.cpp

ubuntu@ubuntu:~$ g++ -lm -lmcrypt -O2 -std=c++11 -pipe -DONLINE_JUDGE -o iostream iostream.cpp

ubuntu@ubuntu:~$ time ./printf < input.txt
221426
real	0m0.497s
user	0m0.480s
sys	0m0.004s

ubuntu@ubuntu:~$ time ./iostream < input.txt
221426
real	0m0.342s
user	0m0.336s
sys	0m0.004s

ubuntu@ubuntu:~$ time ./printf < input.txt 
221426
real	0m0.423s
user	0m0.416s
sys	0m0.004s

ubuntu@ubuntu:~$ time ./iostream < input.txt
221426
real	0m0.347s
user	0m0.340s
sys	0m0.004s

ubuntu@ubuntu:~$ time ./printf < input.txt
221426
real	0m0.420s
user	0m0.412s
sys	0m0.004s

ubuntu@ubuntu:~$ time ./iostream < input.txt
221426
real	0m0.355s
user	0m0.344s
sys	0m0.008s

ubuntu@ubuntu:~$ time ./printf < input.txt
221426
real	0m0.421s
user	0m0.408s
sys	0m0.012s

ubuntu@ubuntu:~$ time ./iostream < input.txt
221426
real	0m0.357s
user	0m0.348s
sys	0m0.004s

ubuntu@ubuntu:~$ time ./printf < input.txt
221426
real	0m0.422s
user	0m0.420s
sys	0m0.000s

ubuntu@ubuntu:~$ time ./iostream < input.txt
221426
real	0m0.356s
user	0m0.348s
sys	0m0.004s

ubuntu@ubuntu:~$ time ./printf < input2.txt
-816737107
real	0m5.779s
user	0m5.664s
sys	0m0.108s

ubuntu@ubuntu:~$ time ./iostream < input2.txt
-816737107
real	0m4.484s
user	0m4.388s
sys	0m0.096s

ubuntu@ubuntu:~$ time ./printf < input2.txt
-816737107
real	0m5.366s
user	0m5.308s
sys	0m0.056s

ubuntu@ubuntu:~$ time ./iostream < input2.txt
-816737107
real	0m5.113s
user	0m5.024s
sys	0m0.088s

ubuntu@ubuntu:~$ time ./printf < input2.txt
-816737107
real	0m5.367s
user	0m5.316s
sys	0m0.052s

ubuntu@ubuntu:~$ time ./iostream < input2.txt
-816737107
real	0m4.478s
user	0m4.416s
sys	0m0.060s

ubuntu@ubuntu:~$ time ./printf < input2.txt
-816737107
real	0m5.362s
user	0m5.280s
sys	0m0.080s

ubuntu@ubuntu:~$ time ./iostream < input2.txt
-816737107
real	0m5.097s
user	0m5.036s
sys	0m0.064s

ubuntu@ubuntu:~$ time ./printf < input2.txt
-816737107
real	0m5.356s
user	0m5.296s
sys	0m0.068s

ubuntu@ubuntu:~$ time ./iostream < input2.txt
-816737107
real	0m4.472s
user	0m4.408s
sys	0m0.068s