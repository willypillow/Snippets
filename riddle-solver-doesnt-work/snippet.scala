// Nationalities // 1 -> English // 2 -> Swedish // 3 -> Danish // 4 -> Norwegian // 5 -> German //
// Colors // 1 -> Red // 2 -> White // 3 -> Green // 4 -> Yellow // 5 -> Blue //
// Pets // 1 -> Dog // 2 -> Bird // 3 -> Cat // 4 -> Horse // 5 -> Fish //
// Drinks // 1 -> Tea // 2 -> Coffee // 3 -> Milk // 4 -> Beer // 5 -> Water //
// Cigarettes // 1 -> Pall Mall // 2 -> Dunhill // 3 -> Blue Master // 4 -> Prince // 5 -> Mixed //

object Main extends App {
  val list = List(1, 2, 3, 4, 5)
  for {nationalities <- list.permutations
    colors <- list.permutations
    pets <- list.permutations
    drinks <- list.permutations
    cigarettes <- list.permutations
  } {
    if (colors(nationalities.indexOf(1)) == 1 &&
      pets(nationalities.indexOf(2)) == 1 &&
      drinks(nationalities.indexOf(3)) == 1 &&
      colors.indexOf(2) - colors.indexOf(3) == 1 &&
      drinks(colors.indexOf(3)) == 2 &&
      pets(cigarettes.indexOf(1)) == 2 &&
      cigarettes(colors.indexOf(4)) == 2 &&
      drinks(2) == 3 &&
      nationalities(0) == 4 &&
      Math.abs(cigarettes.indexOf(5) - pets.indexOf(3)) == 1 &&
      Math.abs(pets.indexOf(4) - cigarettes.indexOf(2)) == 1 &&
      drinks(cigarettes.indexOf(3)) == 4 &&
      cigarettes(nationalities.indexOf(5)) == 4 &&
      Math.abs(nationalities.indexOf(4) - colors.indexOf(5)) == 1 &&
      drinks(cigarettes.indexOf(5)) == 5
      ) {
      println("Nat:" + nationalities)
      println("Col:" + colors)
      println("Pet:" + pets)
      println("Dri:" + drinks)
      println("Cig:" + cigarettes)
    }
  }
}